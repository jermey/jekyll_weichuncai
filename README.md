# jekyll_伪春菜

关于什么事伪春菜,请百度.

这是一个由大神自WordPress移植到emlog 再由我移植到 jekyll的插件.

由于jekyll的特殊性所以使用"[图灵机器人](http://www.tuling123.com)"做后台实现陪聊功能.

### 图灵后台设置

关于怎么注册,怎么创建机器人,获取apikey什么的我就不说了.

后台=>选择机器人=>私有语料库=>默认语料库=>新增

问题:`showlifetime` 答案:`2017-09-16 08:00:00`

答案为创建时间,格式如上所示.

问题:`getnotice` 答案:`系统公告`

答案为系统公告,随意填写.

### 插件使用.

将插件文件放到项目根目录

在footer.html添加如下代码:

```
<!--伪春菜-->
<script>
var weicuncai_key="" //http://www.tuling123.com/ 注册账号获取的apikey
var weicuncai_url="http://www.tuling123.com/openapi/api" //图灵机器人apk调用地址,可以使用七牛\upyun等进行加速实现https
</script>
<link rel="stylesheet" type="text/css" href="{{ site.baseurl }}/weichuncai/css/style.css">
<script src="{{ site.baseurl }}/weichuncai/js/jquery.js"></script>
<script src="{{ site.baseurl }}/weichuncai/js/device.min.js"></script>
<script src="{{ site.baseurl }}/weichuncai/js/common.js"></script>
<script>
	if(!device.mobile()){
		$(document).ready(function() { 
			
			getweichuncai();
			
		}); 
	}
	
	var path = "{{ site.baseurl }}";
	var imagewidth = '240';var imageheight = '240';
	
	var talkself_user = [ ["喔耶～加油！加油！加油！加油！", "1"],["主人现在老是弃旧迎新，朋友们都好伤心啊..", "2"],["哇啊啊啊啊啊啊啊啊啊...", "3"],["主人最帅了", "4"] ];
	talkself_arr = talkself_arr.concat(talkself_user);
	
	createFace("{{ site.baseurl }}/weichuncai/skin/default/face1.gif", "{{ site.baseurl }}/weichuncai/skin/default/face2.gif", "{{ site.baseurl }}/weichuncai/skin/default/face3.gif");
</script>
<!--伪春菜-->
```
